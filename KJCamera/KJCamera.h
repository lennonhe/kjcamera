//
//  KJCamera.h
//  KJCamera
//
//  Created by Lennon He on 20/11/2017.
//  Copyright © 2017 Lennon He. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for KJCamera.
FOUNDATION_EXPORT double KJCameraVersionNumber;

//! Project version string for KJCamera.
FOUNDATION_EXPORT const unsigned char KJCameraVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <KJCamera/PublicHeader.h>
#import <KJCamera/HVideoViewController.h>

